package com.example.farmaciasenturno;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

public class Farmacias extends AppCompatActivity {

    private static final int INTERVALO_VERIFICACION = 5000; // 5 segundos

    private Handler handler;
    private Runnable runnable;


    static ArrayList<Farmacia> farmacias;
    private AdaptadorFarmacias adaptadorFarmacias;
    private ListView lista;

    private RelativeLayout progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmacias);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                verificarConexion();
                handler.postDelayed(this, INTERVALO_VERIFICACION);
            }
        };

        if (isConnected()) {
        } else {
            Toast.makeText(this, "No hay conexión a Internet", Toast.LENGTH_SHORT).show();
            finish();
        }

        farmacias = new ArrayList<Farmacia>();
        lista = (ListView) findViewById(R.id.lista);

        serviceFarmacias();

    }
    private void serviceFarmacias() {
        // progressbar.setVisibility(View.VISIBLE);
        String WS_URL = "https://midas.minsal.cl/farmacia_v2/WS/getLocalesTurnos.php";
        int MY_TIMEOUT_MS = 5000; // Increase the timeout duration (in milliseconds)
        ProgressBar progressBar = findViewById(R.id.progress_bar); // Replace with your ProgressBar ID

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(
                Request.Method.GET,
                WS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            //Farmacia[] farmacias = new Farmacia[jsonArray.length()];
                            ArrayList<Farmacia> farmacias = new ArrayList<>();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Farmacia farmacia = new Farmacia();
                                farmacia.setFecha(jsonObject.getString("fecha"));
                                farmacia.setLocal_id(jsonObject.getString("local_id"));
                                farmacia.setFk_region(jsonObject.getString("fk_region"));
                                farmacia.setFk_comuna(jsonObject.getString("fk_comuna"));
                                farmacia.setFk_localidad(jsonObject.getString("fk_localidad"));
                                farmacia.setLocal_nombre(jsonObject.getString("local_nombre"));
                                farmacia.setComuna_nombre(jsonObject.getString("comuna_nombre"));
                                farmacia.setLocalidad_nombre(jsonObject.getString("localidad_nombre"));
                                farmacia.setLocal_direccion(jsonObject.getString("local_direccion"));
                                farmacia.setFuncionamiento_hora_apertura(jsonObject.getString("funcionamiento_hora_apertura"));
                                farmacia.setFuncionamiento_hora_cierre(jsonObject.getString("funcionamiento_hora_cierre"));
                                farmacia.setLocal_telefono(jsonObject.getString("local_telefono"));
                                farmacia.setLocal_lat(jsonObject.getString("local_lat"));
                                farmacia.setLocal_lng(jsonObject.getString("local_lng"));
                                farmacia.setFuncionamiento_dia(jsonObject.getString("funcionamiento_dia"));

                                if (3 == Integer.parseInt(farmacia.getFk_region())) {
                                    farmacias.add(farmacia);
                                }
                            }
                            progressBar.setVisibility(View.GONE); // Hide the ProgressBar

				/*ACÁ DEBE OBTENER EL JSON ARRAY Y LOS JSON OBJECT, LUEGO APLICA LA CONDICIÓN DE FILTRADO
				Y RECORRE LO OBTENIDO CON UNA ESTRUCTURA CÍCLICA DONDE VA CREANDO NUEVAS FARMACIAS Y LES VA ASIGNANDO LOS
				RESPECTIVOS ATRIBUTOS, FINALMENTE LAS AGREGA A SU ARRAY */

                            /*POSTERIORMENTE LE PASA EL ARRAY AL RESPECTIVO ADAPTADOR Y FINALMENTE LE ASIGNA EL ADAPTADOR A LA LISTA DEL 				LAYOUT */

                            /*SI TODO SALE BIEN DEBE HACER DESAPARECER EL PROGRESS BAR UNA VEZ SE TERMINA EL PROCESO*/

                            adaptadorFarmacias = new AdaptadorFarmacias(farmacias, Farmacias.this);
                            lista.setAdapter(adaptadorFarmacias);

                        } catch (Exception e) {
                            ProgressBar progressBar = findViewById(R.id.progress_bar); // Replace with your ProgressBar ID

                            /* SI FALLA TAMBIÉN DEBE OCULTAR EL PROGRESS BAR PERO LANZAR EL MENSAJE DE ERROR */
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                /* SI FALLA TAMBIÉN DEBE OCULTAR EL PROGRESS BAR PERO LANZAR EL MENSAJE DE ERROR */
                Toast.makeText(Farmacias.this, "Error en el web service! :(", Toast.LENGTH_SHORT).show();
            }
        }
        );
        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(request);
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnable, INTERVALO_VERIFICACION);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    private void verificarConexion() {
        if (isConnected()) {
        } else {
            Toast.makeText(this, "No hay conexión a Internet", Toast.LENGTH_SHORT).show();
            finish(); // Cerrar la aplicación
        }
    }

    private boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}