package com.example.farmaciasenturno;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

/**
 * Created by ProfeAlejandro on 22/06/2023.
 */
/*Clase llamada AdaptadorFarmacias la cual sirve para formatear los datos del array llamado "listaFarmacias", que contiene
        los objetos tipo "Farmacia" extraidos de la web a traves del JSON, donde los lista de manera organizada en el ListView designado.*/
public class AdaptadorFarmacias extends BaseAdapter{

    /*Array de tipo farmacia para el AdaptadorFarmacias y su respectivo context*/
    private ArrayList<Farmacia> listaFarmacias;
    private Context context;

    /*Constructor del AdaptadorFarmacias y variables que recibirá*/
    public AdaptadorFarmacias(ArrayList<Farmacia> listaFarmacias, Context context) {
        this.listaFarmacias = listaFarmacias;
        this.context = context;
    }
    /*Función que obtiene la cantidad de objetos en el array*/
    @Override
    public int getCount() {
        return listaFarmacias.size();
    }

    /*Obtiene la posicion de cierto objeto del array*/
    @Override
    public Object getItem(int position) {
        return listaFarmacias.get(position);
    }

    /*Obtiene el item en la posicion i del arreglo*/
    @Override
    public long getItemId(int i) {
        return 0;
    }

    /*Funcion que infla el listview para su respectivo rellenado y lo devuelve adaptado*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = inflater.inflate(
                    R.layout.adaptador,
                    parent,
                    false);
        }

        /*Objeto farmacia del cual se extraeran los atributos requeridos en los TextView*/
        final Farmacia farmacia = (Farmacia) getItem(position);

        /*Seteo de los datos requeridos hacia los TextView correspondientes*/
        TextView local =(TextView) convertView.findViewById(R.id.nombre);
        TextView localidad =(TextView) convertView.findViewById(R.id.localidad);
        TextView direccion =(TextView) convertView.findViewById(R.id.direccion);
        TextView cierre =(TextView) convertView.findViewById(R.id.cierre);
        
        /*Asignación de los datos en los textview declarados mediante los métodos de la clase Farmacia*/
        local.setText(farmacia.getLocal_nombre());
        localidad.setText(farmacia.getLocalidad_nombre());
        direccion.setText(farmacia.getLocal_direccion());
        cierre.setText("Cierre: " + farmacia.getFuncionamiento_hora_cierre());
        
        /*Aquí se debería guardar la latitud y longitud de las farmacias extraidas para lanzarlas como marcador en la app de
         * navegación preferida por el usuario ej:waze, maps,etc, para facilitar la navegación en caso de no saber llegar */

        Button botonIR = (Button) convertView.findViewById(R.id.botonIR);
        botonIR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("geo:" + farmacia.getLocal_lat() + "," + farmacia.getLocal_lng() + "?z=16&q=" + farmacia.getLocal_lat()+ "," + farmacia.getLocal_lng());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(intent);
            }
        });

        Button botonLlamar = (Button) convertView.findViewById(R.id.botonLlamar);
        botonLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent llamar = new Intent(Intent.ACTION_DIAL);
                llamar.setData(Uri.parse("tel:" + farmacia.getLocal_telefono()));
                AdaptadorFarmacias.this.context.startActivity(llamar);
            }
        });
        return convertView;
    }
}
